/* outpcx.c - Output routines for PCX file format
 * Copyright 1998 Hagen Patzke
 *
 * Known Bugs: MAXCOLBYTES-check at line compression
 */

/* local definitions */
static unsigned char *pcx_line; /* line buffer pointer */
static int opt_pcxmark = 255;   /* invert-marker for pcx bitmap */

static void pcx_init ()
{
   pcx_line = (unsigned char *) malloc ( MAXCOLBYTES );
	return;
}/*pcx_init*/

static void pcx_end ()
{
	free( pcx_line );
	return;
}/*pcx_init*/

static void pcx_bop (FILE *fd)
{
	 int i;
	 unsigned char pcx_header[128];
	 for (i=0; i<128; i++) pcx_header[i]=(unsigned char)0;
	 pcx_header[ 0]=10;             /* ZSoft PCX file */
    pcx_header[ 1]=2;              /* Windows Version */
    pcx_header[ 2]=1;              /* PCX run length encoding */
    pcx_header[ 3]=1;              /* number of bits per pixel */
    pcx_header[ 4]=0;              /* Xmin */
    pcx_header[ 5]=0;              /* Xmin */
    pcx_header[ 6]=0;              /* Ymin */
    pcx_header[ 7]=0;              /* Ymin */
    pcx_header[ 8]=(216*8-1)%256;  /* Xmax */
    pcx_header[ 9]=(216*8-1)/256;  /* Xmax */
    pcx_header[10]=(maxline-1)%256;    /* Ymax */
    pcx_header[11]=(maxline-1)/256;    /* Ymax */
    pcx_header[12]=opt_dpi%256;    /* horizontal resolution */
    pcx_header[13]=opt_dpi/256;    /* horizontal resolution */
    pcx_header[14]=opt_dpi%256;    /* vertical resolution */
    pcx_header[15]=opt_dpi/256;    /* vertical resolution */
    /* 16..18 header palette 0 */
    pcx_header[16]=255;              /* R */
    pcx_header[17]=255;              /* G */
    pcx_header[18]=255;              /* B */
    /* 16..18 header palette 1 */
    pcx_header[19]=0;              /* R */
    pcx_header[20]=0;              /* G */
    pcx_header[21]=0;              /* B */
    /* 19....63 = 0 */
    /* 64 = reserved (0) */
    pcx_header[65]=1;              /* Number of planes (1) */
    pcx_header[66]=(216)%256;/* number of bytes per line */
    pcx_header[67]=(216)/256;/* number of bytes per line */
    pcx_header[68]=1;              /* header palette interpretation (bw) */
    pcx_header[69]=0;              /* header palette interpretation (bw) */
    pcx_header[70]=0;              /* Video Screen X - 1 */
    pcx_header[71]=0;              /* Video Screen X - 1 */
    pcx_header[72]=0;              /* Video Screen Y - 1 */
    pcx_header[73]=0;              /* Video Screen Y - 1 */
    /* 74..127 header null padding */
    fwrite (pcx_header, sizeof (char), 128, fd);
}/*pcx_init*/


/*
 * pcx_compress -- compress output into pcx output array
 */
static int pcx_compress ( unsigned char *line, unsigned char *faxline, int faxcnt )
{
    int i, item;

    /* init compression */
    int o_item = -1;    /* old item */
    int rep_cnt = 0;		/* repeat counter */
    int bytecnt = 0;    /* output count */
    
    for ( i=0; i<faxcnt; i++ )
    { 
    	  /* pcx files are RLE compressed only */
        item = faxline[i]^opt_pcxmark;

		  if ( rep_cnt ){
		     if (item == o_item ){
		     	rep_cnt++;
		     	if (rep_cnt >= 63){
           		line[bytecnt++]=rep_cnt+0xC0;
           		line[bytecnt++]=o_item;
           		rep_cnt=0;
           		o_item=-1;
		     	}
		     }
		     else
		     {
		     	if(rep_cnt>1 || o_item > 191){
           		line[bytecnt++]=rep_cnt+0xC0;
		     	}
           		line[bytecnt++]=o_item;
           		rep_cnt=1;
           		o_item=item;
		     }/*endelse*/
		  }
		  else
		  {
		  		o_item=item;
		  		rep_cnt=1;
        }

    }/*for i */
    
 	 if (rep_cnt){
     	if(rep_cnt>1 || o_item > 191){
     		line[bytecnt++]=rep_cnt+0xC0;
     	}
  	   line[bytecnt++]=item;
  		rep_cnt=0;
  		o_item=-1;
  	 }
  	 
    return (bytecnt);
}/*pcx_compress*/


/*
 *  pcx_eop() -- end pcx page
 */
#pragma ARGSUSED
static void pcx_eop (FILE *fd)
{
	/*NOOP - with file, set x/y values */
	return;
}


/*
 *  pcx_newpage( file ) -- output page pcx file
 *
 */

static void pcx_newpage (FILE * fd)
{
    pcx_eop (fd);    /* throw out current page */
    pcx_bop (fd);    /* Initialize for next page */
    lncnt = 0;
    return;
}/*pcx_newpage*/


/*
 *  pcx_writerow -- compress fax row and print
 *
 */
static void pcx_writerow (FILE* fd, unsigned char * faxrow, int count)
{
    int bytecnt;
    bytecnt = pcx_compress( pcx_line, faxrow, count );
    if (bytecnt > MAXCOLBYTES)
    {
        pm_error("Compressed line overflows cache line buffer!",0,0,0,0,0);
        exit(2);
    }

    fwrite (pcx_line, sizeof (char), bytecnt, fd);

    lncnt++;
    if (lncnt > maxline) pcx_newpage (fd);

}/*pcx_writerow*/

