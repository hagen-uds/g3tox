/* g3tox.c - read a CCITT T.4 Group 3 FAX file and produce x output
 * Copyright 1998 Hagen Patzke
 * 
 * gcc -g -o g3lj -O2 g3lj.c; strip g3lj
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  
 * This software is provided "as is" without express or implied warranty.
 * 
 * Written as an answer to g3tolj.c
 * (Sorry, but it is really too slow. And too cumbersome.)
 */

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>

#ifdef DEBUG_ALL
#  define DEBUG_noscaling
#  define DEBUG_rawgetbit
#  define DEBUG_g3getcode
#  define DEBUG_skiptoeol
#  define DEBUG_g3getrl
#  define DEBUG_getfaxbyte
#  define DEBUG_getfaxrow
#  define DEBUG_main
#endif /*DEBUG_ALL*/

#include "g3tox.h"

/* FAX G3 reading routines */
#include "g3read.c"

/* PCL3 output routines */
#include "outlj.c"
/* PCX  output routines */
#include "outpcx.c"
/* switch macros for PCL3/PCX */
#define x_init()           if(opt_format){pcx_init();}else{lj_init();}
#define x_bop(x)           if(opt_format){pcx_bop(x);}else{lj_bop(x);}
#define x_newpage(x)       if(opt_format){pcx_newpage(x);}else{lj_newpage(x);}
#define x_writerow(x,y,z)  if(opt_format){pcx_writerow(x,y,z);}else{lj_writerow(x,y,z);}
#define x_eop(x)           if(opt_format){pcx_eop(x);}else{lj_eop(x);}
#define x_end()            if(opt_format){pcx_end();}else{lj_end();}

/*
 * main -- do the job...
 */
void main ( int argc, char *argv[] )
{
    int argn, rows, wrows, wcolbytes, row, wrow, col, wcol, i;
    int vval;
    unsigned char *faxrow;
    register int nzcol;
    FILE *fd_in;
    FILE *fd_out;

    char *usage =
    " g3tox  [-kludge] [-reversebits] [-scale N] [-aspect N]\n"
    "        [-resolution 75|100|150|300] [-compress 0|1|2] [-pagelength N]\n"
    "        [-duplength N] [-format 0|1] [-output FILENAME] [g3file]\n"
    "        Option names may be abbreviated to a minimum of 2 characters\n"
    "        Defaults: -sc 1.4 -as 1.0 -res 300 -co 2 -pa 10.95 -du 0.7 -fo 0";

    /* Preset option defaults */
    opt_kludge      = 0;       /* flag for extended sync (skip first 3 fax lines */
    opt_reversebits = 0;       /* flag for reversed bit input */
    opt_dpi         = 300;     /* 300 DPI for HP LaserJet and DeskJet printers */
    opt_scale       = 1.4;     /* hor/vert scaling factor (200 -> 300 dpi is 1.2) */
    opt_aspect      = 1.0;     /* vertical-to-horizontal ratio (2.0 for normal mode) */
    opt_pagelength  = 10.95;   /* length of printed area in inches (A4 = 297 mm) */
    opt_duplength   = 0.7;     /* length of duplicated area in inches */
    opt_cmode       = 2;       /* compression default is maximum (RLE) */
    opt_format      = 0;       /* file output format default is LaserJet */
    strcpy(opt_filename,"\0"); /* initial filename */

    /* Check for command line flags */
    argn = 1;
    while (argn < argc && argv[argn][0] == '-' && argv[argn][1] != '\0') {
        if (pm_keymatch (argv[argn], "-kludge", 2))
            opt_kludge = 1;
        else if (pm_keymatch (argv[argn], "-reversebits", 4))
            opt_reversebits = 1;
        else if (pm_keymatch (argv[argn], "-resolution", 4)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%d", &opt_dpi) != 1) 
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-aspect", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%f", &opt_aspect) != 1)
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-scale", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%f", &opt_scale) != 1)
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-compress", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%d", &opt_cmode) != 1)
                pm_usage (usage);
            if ((opt_cmode < 0) || (opt_cmode > 2)) pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-duplength", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%f", &opt_duplength) != 1)
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-pagelength", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%f", &opt_pagelength) != 1)
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-format", 2)) {
            ++argn;
            if (argn == argc || sscanf (argv[argn], "%d", &opt_format) != 1)
                pm_usage (usage);
        } else if (pm_keymatch (argv[argn], "-output", 2)) {
            ++argn;
            if (argn == argc || strncpy (opt_filename,argv[argn],263) < 0)
                pm_usage (usage);
        } else pm_usage (usage);
        argn++;
    }/*while check for flags*/

    /* assign suitable input/output files */
    if (argn < argc) {
        fd_in = pm_openr (argv[argn]);
        argn++;
    } else {
        fd_in = stdin;
    }/*fi*/

    if( opt_filename[0]=='\0' )
    {
        fd_out = stdout;
    }else{
        fd_out = fopen(opt_filename,"wb");
        if (fd_out == NULL)
        {
        		pm_error("(g3tox.c)main: Could not open output file \"%s\".",opt_filename,0,0,0,0);
        		pm_usage (usage);
        		exit(2);
        }
        
    }/*fi open file*/

    if (argn != argc) pm_usage (usage);

    /* *** END OF GETOPTS *** */

#ifdef DEBUG_main
    fprintf(stderr,"[0]");
#endif

    /* Initialize bit extraction field for rawgetbit */
    shfield_init(opt_reversebits);

    g3_vscale = opt_aspect * opt_scale * 100;
    g3_hscale = opt_scale * 100;

    maxline = opt_pagelength * opt_dpi; /* maximum line count to newpage */

    x_init(); x_bop(fd_out);

    if (opt_kludge) {
        /* Skip 3 extra lines to get in sync. */
        skiptoeol (fd_in);
        skiptoeol (fd_in);
        skiptoeol (fd_in);
    }/*opt_kludge*/

	 /* skip first fax line anyway (?) - no, we won't do THAT... */
    /* skiptoeol (fd_in); */

    /* define maximum output size parameters */
    wcolbytes = (((G3MAXPIXHOR * g3_hscale) / 100) + 7) / 8;
    wrows     = 99999; /* so that output eventually ends */

    faxrow =   (unsigned char *)malloc ( MAXCOLBYTES );
    if ( faxrow == (unsigned char *)NULL ){
        pm_error("(g3tolx.c)main: Memory allocation for fax line input buffer failed. ABORT.",0,0,0,0,0);
        exit(1);
    }/*fi mem alloc error*/

    vval = wrow = row = 0;

    while (row < MAXROWS) {

        /*reset faxrow to white*/
        for (col = 0; col < MAXCOLBYTES; col++) { 
            faxrow[col] = PRT_WHITE; 
        }/*for col*/

        while (vval < 100) {
#ifdef DEBUG_main
    fprintf(stderr,"I");
#endif
            if (row < MAXROWS) 
            {   
                col = getfaxrow (fd_in, row, faxrow);
                if (g3_endoffile){ break; }
                if (col < 0){
                     if(col == G3GFR_EOP){ x_newpage(fd_out); } 
                     else 
                     if(col == G3GFR_EOF){ g3_endoffile++; break; } 
                     else 
                     { pm_error("internal poop %d",col,0,0,0,0); }
                     col=0;
                }/*col < 0*/
                /* count white line bytes from right */
                if(opt_format==0){
	                col--;
   	             while ((col > 0) && (faxrow[col] == PRT_WHITE)){ col--; }
      	          col++;
      	       }/*format PCL3 only*/
         	    wcolbytes = col;
            }/* if(row)*/
            vval += g3_vscale;
            row++;
        }/* while vval */

        while (vval >= 100) {
#ifdef DEBUG_main
    fprintf(stderr,"O\n");
#endif
            if (wrow < wrows) {
                x_writerow(fd_out,faxrow,wcolbytes);
                wrow++;
            }
            vval -= 100;
        }/*while*/

        if (g3_endoffile) break;

    }/*while row<MAXROWS*/

    x_eop(fd_out);
    x_end();
    fclose(fd_in);
    fclose(fd_out);
    exit (0);
}/*main*/

/* END OF g3tox.c ***/
