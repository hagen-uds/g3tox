/* outlj.c - Output routines for PCL3 LaserJet Printers
 * Copyright 1998 Hagen Patzke
 */
#define MAXHIST 500        /* maximum length of history cache */
static struct {
    int bytecnt;
    unsigned char *line;
} hist[MAXHIST];
static int maxhist = MAXHIST;
static int histidx = 0;
static int lncnt   = 0;
/* static int maxline = 11 * 300; */

void lj_init ()
{
	 int i;
    /* maximum line cache is opt_duplength inch or MAXHIST lines */
    maxhist = opt_dpi * opt_duplength; 
    if (maxhist > MAXHIST){ maxhist = MAXHIST; }
    for (i = 0; i < maxhist; i++) {
        hist[i].line = (unsigned char *) malloc ( MAXCOLBYTES );
        hist[i].bytecnt = 0 ;
		  if( hist[i].line == (unsigned char *)NULL ){
		      pm_error("(g3tolx.c)main: Memory allocation for history buffer failed. Trying to use smaller buffer.",0,0,0,0,0);
		      maxhist = i-1;
		      if ( maxhist >= 0 ){
		          free(hist[maxhist].line); /* get some space for fax input line */
		      }else{ 
		          maxhist=0;                /* one of the first two allocations failed! */
		      }/*new maxhist*/
		  }/*mem alloc error*/
    }/*for maxhist lines allocate history buffer*/

    histidx = 0;               /* index in history cache */
    lncnt   = 0;               /* printed line count */
	 return;
}/*lj_init*/

void lj_end ()
{
    int i;
    for (i = 0; i < maxhist; i++) {
        free(hist[i].line);
    }
	 return;
}/*lj_end*/

static void lj_bop (FILE * fd)
{
    fprintf (fd,"\033E");             /* Printer reset */
    fprintf (fd,"\033*rB");           /* end raster graphics */
    fprintf (fd,"\033*t%dR", opt_dpi);    /* Set raster graphics resolution */
    fprintf (fd,"\033&l26A");         /* A4 size */
    fprintf (fd,"\033&l0L");          /* Don't skip perforation */
    fprintf (fd,"\033&l0E");          /* Top margin 0 */
    fprintf (fd,"\033*p0x0Y");        /* move to top left of page & set current position */
    fprintf (fd,"\033*r1A");          /* Start raster graphics, relative adressing */
    fprintf (fd,"\033*b%dM", opt_cmode);  /* set compression mode, 0=none, 1=rle, 2=tiff */
    return;
}/*lj_bop*/


/*
 * lj_compress -- compress lj output into laserjet output array
 */
static int lj_compress ( unsigned char *line, unsigned char *faxline, int faxcnt )
{
    int i, item;

    /* init compression */
    int o_item = -1;
    int oo_item = -1;
    signed char *cb = NULL;     /* command byte */
    int bytecnt = 0;            /* output count */
    
    for ( i=0; i<faxcnt; i++ )
    { 
        item = faxline[i];

        if (opt_cmode == 1) { /* rle compression */
            if (item == o_item && ((unsigned char) (line[bytecnt - 2]) < 255)) 
            {
                ++line[bytecnt - 2];
            }
            else
            {
                line[bytecnt++] = 0;    /* repeat count of one */
                line[bytecnt++] = o_item = item;
            }
        }/*rle compression*/
        else
        if (opt_cmode == 2) { /* tiff compression */
            if (item == o_item && cb != NULL && *cb < 1 && *cb > -127) 
            {
                /* if running repeat, continue if same char again */
                --*cb;
            }
            else
            if (cb != NULL && *cb > 0 && o_item == oo_item && o_item == item) 
            {
        /* if running literal see last two same as next and switch to repeat */
                *cb -= 2;
                cb = (signed char *)&line[bytecnt - 2];
                *cb = -2;
            }
            else
            if (cb != NULL && *cb >= 0 && *cb < 127) 
            {
                /* if running literal add next byte to string */
                ++*cb;
                oo_item = o_item;
                line[bytecnt++] = o_item = item;
            }
            else
            {
                /* start new literal */
                cb = (signed char *)&line[bytecnt];
                line[bytecnt++] = 0;          /* repeat count of one */
                line[bytecnt++] = o_item = item;
                oo_item = -1;
            }
        }/*tiff compression*/
        else 
        {
            /* no compression */
            line[bytecnt++] = item;
        }/*compression type*/
    }/*for i */
    return (bytecnt);
}/*lj_compress*/


/*
 *  lj_eop() -- end printing page
 */
static void lj_eop (FILE * fd)
{
    fprintf (fd,"\033*rB");           /* end raster graphics */
    fprintf (fd,"\033E");             /* Printer reset. */
    return;
}


/*
 *  lj_newpage( file ) -- output page to printer
 *
 */

lj_newpage (FILE * fd)
{
    int i;

    lj_eop (fd);      /* Printer End-Of-Job command throws out current page */
    lj_bop (fd);      /* Initialize printer for next page */
    lncnt = 0;
    for (i = 0; i < maxhist; i++) {
        /* output overlap lines from cache buffer */
        fprintf (fd, "\033*b%dW", hist[histidx].bytecnt);
        fwrite (hist[histidx].line, sizeof (char), hist[histidx].bytecnt, fd);
        /* increment line buffer counter with overlap at cache beoundary */
        histidx++;
        if (histidx >= maxhist) { histidx = 0; }

        lncnt++;           /* increment printed line counter */
    }/*for i*/
}/*newpage*/


/*
 *  lj_writerow -- compress fax row and print
 *
 */
static void lj_writerow (FILE* fd, unsigned char * faxrow, int count)
{
    int bytecnt;

    bytecnt = lj_compress( hist[histidx].line, faxrow, count );
    hist[histidx].bytecnt = bytecnt;

    if (bytecnt > MAXCOLBYTES)
    {
        pm_error("Compressed line overflows cache line buffer!",0,0,0,0,0);
        exit(2);
    }

    fprintf (fd, "\033*b%dW", bytecnt);
    fwrite (hist[histidx].line, sizeof (char), bytecnt, fd);

    histidx++;
    if (histidx >= maxhist) histidx = 0;

    lncnt++;
    if (lncnt > maxline) lj_newpage (fd);

}/*lj_writerow*/

