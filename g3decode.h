/* g3decode.h -- G3-decoding header file
 * Copyright 1998 Hagen Patzke
 */
 
/* I do not use a hash function but a binary tree.
 * There are 120 codes for black/white bit runs defined in 
 * CCITT Recommendation T.4 .
 * If I use unsigned characters, this is enough to code 
 * - a pointer to the next 0-node or 1-node
 * - length information for the bit runs
 * - information about length prefixes
 * - for error codes.
 *
 * ENCODING :
 *
 * Values from 1..127 are pointers to the next tree element.
 *
 * 128 logical or-ed with ordinal numbers represent special values:
 *  0...63  : bit runs (0..63 bits in a row)
 *      64  : End-Of-Line code
 * 65..104  : "makeup codes", represent prefix for multiples of 64 bit
 *     127  : marks an illegal bit run
 *
 * Format: new_tree_position=g3d_xxxx[old_tree_position].b[bit];
 */

typedef struct { unsigned char b[2]; } G3NODE;

G3NODE g3d_white[] = {
    {  1,  4}, { 11,  2}, { 14,  3}, { 28,130}, {  5,  8}, 
    {  6,  7}, {131, 16}, { 17,132}, {  9, 10}, {133, 18}, 
    {134,135}, { 19, 12}, { 24, 13}, { 41,138}, { 15, 26}, 
    {139, 43}, {194,136}, {137, 30}, { 31,193}, { 20, 22}, 
    { 32, 21}, { 34,141}, { 35, 23}, { 37,129}, { 25, 39}, 
    {140, 38}, { 46, 27}, { 72,195}, { 29, 49}, {218, 75}, 
    {144,145}, {142,143}, { 51, 33}, { 53,150}, {151, 54}, 
    { 36, 56}, {148, 55}, {147, 59}, { 60,154}, { 61, 40}, 
    { 64,149}, { 42, 66}, {156, 65}, { 44, 45}, {155, 69}, 
    { 80,146}, { 47, 48}, {152, 70}, { 71,153}, { 78, 50}, 
    { 88,196}, { 91, 52}, {157,158}, {173,174}, {175,176}, 
    {161,162}, { 57, 58}, {163,164}, {165,166}, {159,160}, 
    {181,182}, { 62, 63}, {167,168}, {169,170}, {171,172}, 
    {189,190}, { 67, 68}, {191,128}, {197,198}, {187,188}, 
    {177,178}, {179,180}, { 73, 74}, {183,184}, {185,186}, 
    { 76, 77}, {199,200}, { 83,202}, { 79, 85}, {201, 84}, 
    { 81, 82}, {215,216}, {217,219}, {203,204}, {205,206}, 
    { 86, 87}, {207,208}, {209,210}, { 89, 90}, {211,212}, 
    {213,214}, { 97, 92}, { 93, 95}, { 94,102}, {220,101}, 
    { 96,105}, {221,222}, { 98,255}, { 99,255}, {100,255}, 
    {100,192}, {223,224}, {103,104}, {225,226}, {227,228}, 
    {106,107}, {229,230}, {231,232} 
};/*g3d_white*/


G3NODE g3d_black[] = {
    {  2,  1}, {131,130}, {  4,  3}, {129,132}, {  6,  5}, 
    {134,133}, {  9,  7}, {  8,135}, {137,136}, { 13, 10}, 
    { 11, 12}, {138,139}, { 17,140}, { 19, 14}, { 15, 16}, 
    {141, 25}, { 27,142}, { 18, 29}, {143, 45}, { 31, 20}, 
    { 21, 23}, { 22, 37}, {146, 61}, { 39, 24}, { 72,193}, 
    { 41, 26}, { 78,144}, { 28, 43}, {145, 81}, { 47, 30}, 
    { 49,128}, { 50, 32}, { 33, 35}, { 34, 55}, {220, 54}, 
    { 36, 58}, {221,222}, { 64, 38}, { 67,152}, { 40, 69}, 
    {153, 68}, { 42, 75}, {151, 74}, { 84, 44}, { 87,150}, 
    { 88, 46}, { 91,147}, { 48, 93}, {148, 92}, {149, 96},
    { 51,255}, { 52,255}, { 53,255}, { 53,192}, {223,224},
    { 56, 57}, {225,226}, {227,228}, { 59, 60}, {229,230}, 
    {231,232}, { 62, 63}, {180, 97}, { 98,183}, { 65, 66}, 
    {184, 99}, {100,187}, {188,101}, {102,197}, { 70, 71}, 
    {198,199}, {103,181}, { 73,105}, {182,104}, {178,179}, 
    { 76, 77}, {172,173}, {174,175}, { 79, 80}, {185,186}, 
    {189,196}, { 82, 83}, {176,177}, {190,191}, { 85, 86}, 
    {158,159}, {160,161}, {168,169}, { 89, 90}, {194,195}, 
    {154,155}, {156,157}, {162,163}, { 94, 95}, {164,165}, 
    {166,167}, {170,171}, {202,203}, {204,205}, {212,213}, 
    {214,215}, {216,217}, {218,219}, {200,201}, {206,207},
    {106,107}, {208,209}, {210,211}
};/*g3d_black*/

/* Note:
 * g3d_white node(100) changed from (0,192)->(100,192)
 *           to add support for "filler" bits in FAX code
 * g3d_black node(53) changed to (53,192), accordingly
 * So no special program code to suppress "filler bits" is needed.
 */
 
/*
 * CCITT g3 (FAX) management definitions
 *
 * Recommendation T.4 for Group 3 Facsimile Transmission states
 * - 1728 pels represent a 215 mm scan line (204.145 dpi)
 * - 2048 pels represent a 255 mm scan line (203.997 dpi)
 * - 2432 pels represent a 303 mm scan line (203.871 dpi)
 * - 7.7 lines represent 1 mm               (195.580 dpi)
 * (at normal resolution each transmitted line is duplicated,
 *  giving 98 dpi)
 *
 * The "Guaranteed reproducible area for Group 3 apparatus
 * conforming to Recommendation T.4" is 
 * - 196.6mm horizontal * 281.46mm vertical
 * - on a 210mm * 297mm A4 page.
 * 
 */
#define G3MAXPIXHOR 2432  /* 1728 (A4), 2048 or 2432 pels */
#define G3EOL       64    /* Index of end-of-line code */
#define G3MAKEUP    64    /* markup multiplier base (step=64 pels) */
#define G3MAXMAKEUP 40    /* biggest markup value (=2560 pels) */
#define G3ILLEGAL   127   /* illegal code value */

#define G3DPIHOR    204   /* horizontal resolution of fax apparatus */
#define G3DPIVERT   196   /* vertical   resolution of fax apparatus */

#define G3_WHITE    0     /* white on G3-paper is "bit is not set" */
#define G3_BLACK    1     /* black on G3-paper is "bit is set" */

#define PRT_WHITE   0x00  /* white on paper is 00 */
#define PRT_BLACK   0xFF  /* black on paper is FF */


/* END of g3decode.h ***/