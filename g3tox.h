/* g3tox.h - common definitions
 * Copyright 1998 Hagen Patzke
 */

/* FAX G3 definitions */
#include "g3decode.h"

int g3_color = G3_WHITE;  /* alternating flag for b/w runs */
int g3_run   = 0;         /* length of g3 run */
int g3_endoffile = 0;	  /* end of file flag */
int g3_eols = 0;

/* Constants - stolen from g3tolj.c */
#define pm_error(a,b,c,d,e,f) {fprintf(stderr,a,b,c,d,e,f);fprintf(stderr,"\n");}
#define pm_message(a,b,c,d,e,f) {fprintf(stderr,a,b,c,d,e,f);fprintf(stderr,"\n");}
#define pm_usage(a) {fprintf(stderr,"usage: %s\n",a);exit(7);}

/* g3tox helper functions */
#include "g3helper.c"

/*
 * Common global values
 */
static int g3_hscale   = 100;     /* horizontal scaling factor x 100 */
static int g3_vscale   = 100;     /* vertical scaling factor   x 100 */
static int g3_getbytes = 216;     /* 1728 (A4) pels to get per line */

#define MAXROWS     43000      /* up to 20 pages long */
#define MAXCOLBYTES 400        /* max print column bytes (=270mm at 300dpi!) */

static int maxline = 11 * 300; /* max # print lines */

static int opt_dpi         = 300; /* nominal printer resolution */
static int opt_cmode       = 0;   /* compression mode, 0=none, 1=rll, 2=tiff */
static int opt_kludge      = 0;   /* flag for skipping at begin of file */
static int opt_reversebits = 0;   /* flag for reverse bit order */
static int opt_format      = 0;   /* file formet, 0=PCL3, 1=PCX */
static float opt_duplength = 0.7; /* overlap length (printer) */
static float opt_aspect;
static float opt_scale;
static float opt_pagelength;
char opt_filename[264];           /* file name */

/* skiptoeol is fulfilled if getfaxbyte has done it! */
#define G3GF_RESET    0x00
#define G3GF_HANGEOL  0x01
#define G3GF_MAKEUP   0x02
static int getfaxbyte_status = G3GF_RESET;

/* defines for getfaxrow */
#define G3GFR_EOL		-1
#define G3GFR_EOP		-2
#define G3GFR_EOF		-3

