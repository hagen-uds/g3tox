/* g3read.c -- FAX G3 reader routines 
 * Copyright 1998 Hagen Patzke
 */

/*
 * g3getcode - get g3 bit run code information
 */
int g3getcode(FILE *file)
{
    G3NODE *g3;
    int    idx=0;
    int    b;

    if( g3_endoffile ){ return (G3EOL); }

    /*init pointer to correct decoding tree*/
    if(g3_color==G3_WHITE){
        g3 = g3d_white;
    }else{
        g3 = g3d_black;
    }/*fi*/
#ifdef DEBUG_g3getcode
   fprintf(stderr,"[");
#endif

    /*walk g3 decoder tree until you come to a leaf*/
    do{
        b=rawgetbit(file);
        if(b < 0){
            idx=(G3EOL|128);      /* End-Of-File or file error */
        }else{
            idx=g3[idx].b[b];     /* walk g3 decoding tree */
        }/*if b<0*/
    }while(idx<128);

    idx &= 0x7f;

    if(idx==G3EOL){g3_eols++;}

#ifdef DEBUG_g3getcode
   fprintf(stderr,"%02x] ",idx);
#endif

    return (idx); 
}/*g3getcode*/

/*
 * skiptoeol() - read g3codes till eol
 */
void skiptoeol(FILE *file)
{
/* skiptoeol is fulfilled if getfaxbyte has done it! */
#define G3GF_RESET    0x00
#define G3GF_HANGEOL  0x01
#define G3GF_MAKEUP   0x02
static int getfaxbyte_status = G3GF_RESET;
#ifdef DEBUG_skiptoeol
   int i;
   fprintf(stderr,"S(");
#endif

   if ( getfaxbyte_status == G3GF_HANGEOL ){
       getfaxbyte_status = G3GF_RESET;
   } else {
       while( G3EOL != g3getcode(file) ){ 
           /*NOOP*/ 
#ifdef DEBUG_skiptoeol
           i++;
#endif
       }/*while*/
   }/*fi getfaxbyte_status*/
#ifdef DEBUG_skiptoeol
   fprintf(stderr,"%d)",i);
#endif
   return;
}/*skiptoeol*/


/* 
 * g3getrl() -- get fax run length from g3 codes
 * returns : 
 *      0..xxxx : processed fax byte value
 *           -1 : fax scan End-Of-Line
 *           -2 : EOP
 *           -3 : EOF
 */
int g3getrl( FILE *file )
{
    int   code;
    int   g3_runlen;

#ifdef DEBUG_g3getrl
    fprintf(stderr,"&");
#endif

    if ( g3_endoffile ){ return(-1); };  /* EOF -> EOL */

    g3_runlen = 0;

    /* let us get a byte */
    code = g3getcode(file);

    if ( code < G3EOL ) {                  /* valid bit run ? */
        g3_runlen = code;                     /* bit run 0..63 */
    } else  
    if ( code == G3EOL ) {                 /* End-Of-Line code ? */
        g3_runlen = -1;
    } else  
    if ( code <= (G3MAXMAKEUP+G3EOL) ) {   /* Makeup code ? */
        g3_runlen = G3MAKEUP * (code - G3EOL);

        code = g3getcode(file);
        if ( code < G3EOL ) { 
            g3_runlen += code; 
        } else { 
            pm_error("\nIllegal makeup/code pair (%d pels followed by code 0x%02x)",g3_run,code,0,0,0);
            skiptoeol(file);
            g3_runlen = -1;
        }/*if code */

    } else {                              /* illegal value! */
        pm_error("\nIllegal code value 0x%02x",code,0,0,0,0);
        skiptoeol(file);
        g3_runlen = -1;
    }/*fi code */

#ifdef DEBUG_g3getrl
    fprintf(stderr,"->%d&",g3_run);
#endif
    return (g3_runlen);
}/*g3getrl()*/

/* 
 * getfaxbyte() -- get fax byte from g3 codes
 *               - includes scaling
 *               - includes EOF detection
 * returns : 
 *           0..255 : processed fax byte value
 *               -1 : fax scan End-Of-Line
 */
int getfaxbyte(FILE *file)
{
    static int hval              = 0;
    static int oldcode           = 0;
    static int eopdet            = 0;

    int shdata;
    int shbit;
    int count;
    int code;

#ifdef DEBUG_getfaxbyte
    fprintf(stderr,"b");
#endif

    if ( g3_endoffile ){ return(G3GFR_EOF); };

    /* let us get a byte */
    shdata = 0;
    shbit  = 0x80;
    count  = 8;
    while( count-- ){

        /* routine: "getprocessedbit" - only used here, so it stays here */

        while( g3_run == 0){

            code = g3getrl(file);

            if ( code == 0 ) {                     /* bit run 0 -> change color */
                g3_color = ( g3_color == G3_WHITE ) ? G3_BLACK : G3_WHITE;
                getfaxbyte_status = G3GF_RESET;
                eopdet=0;
            } else  /* valid bit run ? */
            if ( code > 0 ) {
                g3_run = code;                     /* bit run 1..xxx */
                getfaxbyte_status = G3GF_RESET;
                eopdet=0;
            } else  /* End-Of-Line code ? */
            if ( code < 0 ) {
            
                eopdet++;                     /*end-of-page detector*/
                if(eopdet>5){
                    return (G3GFR_EOF);  /* end of fax file */
                }else if(eopdet>2){
                    return (G3GFR_EOP);  /* end of fax page */
                }else{
                    return (G3GFR_EOL);  /* eol */
                }/*eopdet*/

                g3_color = G3_WHITE;
                getfaxbyte_status = G3GF_RESET;
                oldcode = code;
                /* runlen and scale reset */
                g3_run = 0;
                hval = 0;

                return(shdata);

            }/*fi code*/

        }/*while(g3_run==0)*/

        /* set bit if run > 0 and black color is printed */
        if(g3_color == G3_BLACK){ 
            shdata |= shbit; 
        }
        shbit >>= 1;

        /* x scaling takes place in (not) counting down*/
#ifdef DEBUG_noscaling
        hval -= 100;
        while( hval < 100){
            hval += g3_hscale;
#endif
            /* count down bit run counter */
            g3_run--;
            if( g3_run == 0){
                /* NOW change color */
                g3_color = ( g3_color == G3_WHITE ) ? G3_BLACK : G3_WHITE;
            }/*color change if bit run expletes */
#ifdef DEBUG_noscaling
        }/*while hval < 100 */
        /* end x scaling */
#endif

    }/*while (bit-) count*/

    return (shdata);

}/*getfaxbyte*/


/*
 * getfaxrow  - get fax byte row from g3 file
 *
 */
int getfaxrow ( FILE *file, int row, unsigned char *faxrow )
{
    int count;
    int fb=G3GFR_EOL; 
    /* flag byte fb:
       G3GFR_EOL == end-of-line
       G3GFR_EOP == end-of-page
       G3GFR_EOF == end-of-file
       -> all set by getfaxbyte (see above)
    */
    int endflag=0;
#ifdef DEBUG_getfaxrow
    fprintf(stderr,"<R");
#endif

    if (g3_endoffile){ return(G3GFR_EOF); }

    /* reset fax row to white */
    for (count = 0; count < MAXCOLBYTES; count++){ 
        faxrow[count] = PRT_WHITE;
    }/*for*/

    g3_color = G3_WHITE;
    for (count = 0; count < MAXCOLBYTES; count++){ 
        fb = getfaxbyte(file);
        if ( fb >= 0 ){
            faxrow[count] = fb;
        }else if(fb == G3GFR_EOL){
            endflag++;
#ifdef DEBUG_getfaxrow
            fprintf(stderr,"[EOL %d, read bytes %d]",g3_eols,count);
#endif
            fb = count;
            break;
        }else{
            endflag++;
            /* NOOP getfaxbyte returns -1 EOL, -2 EOP, -3 EOF */
#ifdef DEBUG_getfaxrow
            if (fb == G3GFR_EOP) {
        	   fprintf(stderr,"[EOP]");
            }else if (fb == G3GFR_EOF) {
        	   fprintf(stderr,"[EOF]");
            }/*fi*/
#endif
            break;
        }/* if fax byte received ok */
    }/*for fill faxrow*/
    
    if(endflag==0){
        pm_message("Fax scan line #%d too long.",g3_eols,0,0,0,0);
        fb=MAXCOLBYTES;
#ifdef DEBUG_getfaxrow
            fprintf(stderr,"*skip*");
#endif
        skiptoeol(file);
    }/*fi endflag*/
#ifdef DEBUG_getfaxrow
    fprintf(stderr,"R>");
#endif
    
    return (fb);
}/*getfaxrow*/


/* END OF g3read.c ***/